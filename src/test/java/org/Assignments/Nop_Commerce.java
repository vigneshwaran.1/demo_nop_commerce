package org.Assignments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Nop_Commerce {

	public static WebDriver driver;

	@BeforeClass
	public void launchBrowser() {
		driver = new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}

	@Test
	public void login() {

		driver.findElement(By.xpath("//button[@class='button-1 login-button']")).click();

		String s1 = driver.findElement(By.partialLinkText("John Smith")).getText();
		Assert.assertEquals("John Smith", s1);

	}

	@Test(priority = 1)

	public void Categories() throws InterruptedException, IOException {

		File f = new File("/home/vigneshwaran/Documents/Exceldata/ed.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheetAt(0);
		int rows = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			driver.findElement(By.xpath("//p[contains(text(),' Categories')]")).click();
			driver.findElement(By.xpath("//i[@class='fas fa-plus-square']")).click();
			String name = sheet.getRow(i).getCell(0).getStringCellValue();
			String description = sheet.getRow(i).getCell(0).getStringCellValue();
			String numericCellValue = sheet.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			WebElement parentcatalog = driver.findElement(By.id("ParentCategoryId"));
			Select s = new Select(parentcatalog);
			s.selectByVisibleText("Computers >> Build your own computer");
			WebElement price = driver.findElement(By.xpath("//label[@for=\"PriceFrom\"]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView()", price);
		}
	}

	
  @Test(priority = 2)
	public void Products() {
		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement dd = driver.findElement(By.id("SearchCategoryId"));
		Select s = new Select(dd);
		s.selectByIndex(2);
		driver.findElement(By.id("search-products")).click();

	}
	@Test(priority = 3)
	public void Manufacturers() throws IOException {
		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
		File f = new File("/home/vigneshwaran/Documents/Exceldata/ed.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sh = workbook.getSheetAt(1);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String srh1 = sh.getRow(i).getCell(0).getStringCellValue();
			String pub = sh.getRow(i).getCell(0).getStringCellValue();
			driver.findElement(By.id("SearchManufacturerName")).sendKeys(srh1);
			driver.findElement(By.id("SearchPublishedId")).sendKeys(pub);
			driver.findElement(By.xpath("//button[@name='save']")).click();
		}
	}
	@Test(priority = 4)
	public void Logout() {
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}
	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
